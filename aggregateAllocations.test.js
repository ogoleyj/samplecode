const chai = require("chai");
const expect = chai.expect;
const hashmap = require("hashmap");
const NodeRules = require("node-rules");
const AggregateAllocationsRule = require("../../config/controller-rules/aggregateAllocations");
const AggregateAllocationsStubData = require("./test-data/aggregateAllocations-rules-data-sample");

describe("AggregateAllocations Class Test", () => {
  describe("Scenario 1: TM/SWIPR responds but there are no actual pending transactions", () => {
    let rulesInput = new hashmap();
    // Set all MS inside rules input so we can pass these to rule
    rulesInput.set(
      "PendingInvestmentElectionsData",
      AggregateAllocationsStubData.s1_PendingInvestmentElectionsData()
    );
    rulesInput.set(
      "InvestmentsData",
      AggregateAllocationsStubData.InvestmentsData()
    );
    rulesInput.set(
      "PlanSourcesData",
      AggregateAllocationsStubData.PlanSourcesData()
    );
    rulesInput.set(
      "ParticipantPlanInvestmentAllocationsData",
      AggregateAllocationsStubData.ParticipantPlanInvestmentAllocationsData()
    );
    rulesInput.set(
      "ParticipantPlanAccountData",
      AggregateAllocationsStubData.ParticipantPlanAccountsData()
    );

    // Call rule
    this.nodeRules = new NodeRules(AggregateAllocationsRule, {
      ignoreFactChanges: true
    });
    it("AggregateAllocations rule should return UnweightedAverage with the proper fields and classes", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let UnweightedAverage = result.get("UnweightedAverage");
        let hasProps = false;
        let firstElement = UnweightedAverage.data[1];
        // Check if Unweighted Average has the fields that it should
        if (
          firstElement.assetClassDesc &&
          firstElement.assetClassAllocationTotal &&
          firstElement.CategoryAllocations &&
          firstElement.CategoryAllocations[1].assetCategoryGroupName &&
          firstElement.CategoryAllocations[1].assetCategoryGroupAllocationTotal
        ) {
          hasProps = true;
        }
        expect(hasProps).to.equal(true);
      });
    });
    it("AggregateAllocations rule should return UnweightedAverage with source 'B' having 50% in the 'Bond' assetClass", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let UnweightedAverage = result.get("UnweightedAverage");
        let bondInfo = UnweightedAverage.data.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(50);
      });
    });
    it("AggregateAllocations rule should return AggregateSources array with 5 element", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        expect(AggregateSources.data.length).to.equal(5);
      });
    });
    it("AggregateAllocations rule should return AggregateSources with source 'B' having '50%' in the 'Bond' assetClassDesc", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let singleSource = AggregateSources.data.filter(
          source => source.sourceTypeCd == "B"
        );
        let bondInfo = singleSource[0].ClassAllocations.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(50);
      });
    });
    it("AggregateAllocations rule should return Investments for source '9' having '50%' where agreementInvestmenProductId = 106674", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let Investments = result.get("Investments");
        let singleInvestment = Investments.data.filter(source => {
          return source.agreementInvestmentProductId == "106674";
        });
        let investmentSourceInfo = singleInvestment[0].InvestmentSources.filter(
          source => {
            return source.sourceTypeCd == "9";
          }
        );
        expect(investmentSourceInfo[0].investmentElectionPct).to.equal(50);
      });
    });
    it("AggregateAllocations rule should return lastAllocationChangeDt from EDWIN of 2020-05-15", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let lastAllocationChangeDt = result.get(
          "LastAllocationPercentageChangeDt"
        );
        expect(lastAllocationChangeDt.data).to.equal(
          "2020-05-15T00:00:00.000Z"
        );
      });
    });
  });

  describe("Scenario 2: TM/SWIPR MS responded with 404, pull from EDWIN", () => {
    let rulesInput = new hashmap();
    // Set all MS inside rules input so we can pass these to rule
    rulesInput.set(
      "PendingInvestmentElectionsData",
      AggregateAllocationsStubData.s2_PendingInvestmentElectionsData()
    );
    rulesInput.set(
      "InvestmentsData",
      AggregateAllocationsStubData.InvestmentsData()
    );
    rulesInput.set(
      "PlanSourcesData",
      AggregateAllocationsStubData.PlanSourcesData()
    );
    rulesInput.set(
      "ParticipantPlanInvestmentAllocationsData",
      AggregateAllocationsStubData.ParticipantPlanInvestmentAllocationsData()
    );
    rulesInput.set(
      "ParticipantPlanAccountData",
      AggregateAllocationsStubData.ParticipantPlanAccountsData()
    );

    // Call rule
    this.nodeRules = new NodeRules(AggregateAllocationsRule, {
      ignoreFactChanges: true
    });
    it("AggregateAllocations rule should return UnweightedAverage with the proper fields and classes", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let UnweightedAverage = result.get("UnweightedAverage");
        expect(AggregateSources.data.length).to.equal(5);
        expect(UnweightedAverage.data.length).to.greaterThan(0);
        let hasProps = false;
        let firstElement = UnweightedAverage.data[1];
        // Check if Unweighted Average has the fields that it should
        if (
          firstElement.assetClassDesc &&
          firstElement.assetClassAllocationTotal &&
          firstElement.CategoryAllocations &&
          firstElement.CategoryAllocations[1].assetCategoryGroupName &&
          firstElement.CategoryAllocations[1].assetCategoryGroupAllocationTotal
        ) {
          hasProps = true;
        }
        expect(hasProps).to.equal(true);
      });
    });
    it("AggregateAllocations rule should return UnweightedAverage with source 'B' having 50% in the 'Bond' assetClass", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let UnweightedAverage = result.get("UnweightedAverage");
        let bondInfo = UnweightedAverage.data.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(50);
      });
    });
    it("AggregateAllocations rule should return AggregateSources array with 5 element", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        expect(AggregateSources.data.length).to.equal(5);
      });
    });
    it("AggregateAllocations rule should return AggregateSources with source 'B' having '50%' in the 'Bond' assetClassDesc", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let singleSource = AggregateSources.data.filter(
          source => source.sourceTypeCd == "B"
        );
        let bondInfo = singleSource[0].ClassAllocations.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(50);
      });
    });
    it("AggregateAllocations rule should return Investments for source '9' having '50%' where agreementInvestmenProductId = 106674", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let Investments = result.get("Investments");
        let singleInvestment = Investments.data.filter(source => {
          return source.agreementInvestmentProductId == "106674";
        });
        let investmentSourceInfo = singleInvestment[0].InvestmentSources.filter(
          source => {
            return source.sourceTypeCd == "9";
          }
        );
        expect(investmentSourceInfo[0].investmentElectionPct).to.equal(50);
      });
    });
    it("AggregateAllocations rule should return lastAllocationChangeDt from EDWIN of 2020-05-15", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let lastAllocationChangeDt = result.get(
          "LastAllocationPercentageChangeDt"
        );
        expect(lastAllocationChangeDt.data).to.equal(
          "2020-05-15T00:00:00.000Z"
        );
      });
    });
  });

  describe("Scenario 3: TM/SWIPR MS did respond, with a pending 'investmentelections' change", () => {
    let rulesInput = new hashmap();
    // Set all MS inside rules input so we can pass these to rule
    rulesInput.set(
      "PendingInvestmentElectionsData",
      AggregateAllocationsStubData.s3_PendingInvestmentElectionsData()
    );
    rulesInput.set(
      "InvestmentsData",
      AggregateAllocationsStubData.InvestmentsData()
    );
    rulesInput.set(
      "PlanSourcesData",
      AggregateAllocationsStubData.PlanSourcesData()
    );
    rulesInput.set(
      "ParticipantPlanInvestmentAllocationsData",
      AggregateAllocationsStubData.ParticipantPlanInvestmentAllocationsData()
    );
    rulesInput.set(
      "ParticipantPlanAccountData",
      AggregateAllocationsStubData.ParticipantPlanAccountsData()
    );

    // Call rule
    this.nodeRules = new NodeRules(AggregateAllocationsRule, {
      ignoreFactChanges: true
    });
    it("AggregateAllocations rule should return UnweightedAverage with the proper fields and classes", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let UnweightedAverage = result.get("UnweightedAverage");
        expect(AggregateSources.data.length).to.equal(5);
        expect(UnweightedAverage.data.length).to.greaterThan(0);
        let hasProps = false;
        let firstElement = UnweightedAverage.data[0];
        // Check if Unweighted Average has the fields that it should
        if (
          firstElement.assetClassDesc &&
          firstElement.assetClassAllocationTotal &&
          firstElement.CategoryAllocations &&
          firstElement.CategoryAllocations[0].assetCategoryGroupName &&
          firstElement.CategoryAllocations[0].assetCategoryGroupAllocationTotal
        ) {
          hasProps = true;
        }
        expect(hasProps).to.equal(true);
      });
    });
    it("AggregateAllocations rule should return UnweightedAverage with source 'B' having 100% in the 'Bond' assetClass", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let UnweightedAverage = result.get("UnweightedAverage");
        let bondInfo = UnweightedAverage.data.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(100);
      });
    });
    it("AggregateAllocations rule should return AggregateSources array with 5 element", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        expect(AggregateSources.data.length).to.equal(5);
      });
    });
    it("AggregateAllocations rule should return AggregateSources with source 'B' having '100%' in the 'Bond' assetClassDesc", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let singleSource = AggregateSources.data.filter(
          source => source.sourceTypeCd == "B"
        );
        let bondInfo = singleSource[0].ClassAllocations.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(100);
      });
    });
    it("AggregateAllocations rule should return Investments for source '9' having '50%' where agreementInvestmenProductId = 106674", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let Investments = result.get("Investments");
        let singleInvestment = Investments.data.filter(source => {
          return source.agreementInvestmentProductId == "121809";
        });
        let investmentSourceInfo = singleInvestment[0].InvestmentSources.filter(
          source => {
            return source.sourceTypeCd == "9";
          }
        );
        expect(investmentSourceInfo[0].investmentElectionPct).to.equal(100);
      });
    });
    it("AggregateAllocations rule should return lastAllocationChangeDt from EDWIN of 2020-05-12", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let lastAllocationChangeDt = result.get(
          "LastAllocationPercentageChangeDt"
        );
        expect(lastAllocationChangeDt.data).to.equal("2020-05-12T17:39:33Z");
      });
    });
  });

  describe("Scenario 4: TM/SWIPR MS responded with a fatal error", () => {
    let rulesInput = new hashmap();
    // Set all MS inside rules input so we can pass these to rule
    rulesInput.set(
      "PendingInvestmentElectionsData",
      AggregateAllocationsStubData.s4_PendingInvestmentElectionsData()
    );
    rulesInput.set(
      "InvestmentsData",
      AggregateAllocationsStubData.InvestmentsData()
    );
    rulesInput.set(
      "PlanSourcesData",
      AggregateAllocationsStubData.PlanSourcesData()
    );
    rulesInput.set(
      "ParticipantPlanInvestmentAllocationsData",
      AggregateAllocationsStubData.ParticipantPlanInvestmentAllocationsData()
    );
    rulesInput.set(
      "ParticipantPlanAccountData",
      AggregateAllocationsStubData.ParticipantPlanAccountsData()
    );

    // Call rule
    this.nodeRules = new NodeRules(AggregateAllocationsRule, {
      ignoreFactChanges: true
    });
    it("AggregateAllocations return empty arrays since TM/SWIPR errored out", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let UnweightedAverage = result.get("UnweightedAverage");
        expect(AggregateSources.data.length).to.equal(0);
        expect(UnweightedAverage.data.length).to.equal(0);
      });
    });
  });

  describe("Scenario 5: EDWIN responded with a fatal error", () => {
    let rulesInput = new hashmap();
    // Set all MS inside rules input so we can pass these to rule

    rulesInput.set(
      "PendingInvestmentElectionsData",
      AggregateAllocationsStubData.s3_PendingInvestmentElectionsData()
    );
    rulesInput.set(
      "InvestmentsData",
      AggregateAllocationsStubData.FailedEdwinResponse()
    );
    rulesInput.set(
      "PlanSourcesData",
      AggregateAllocationsStubData.PlanSourcesData()
    );
    rulesInput.set(
      "ParticipantPlanInvestmentAllocationsData",
      AggregateAllocationsStubData.ParticipantPlanInvestmentAllocationsData()
    );
    rulesInput.set(
      "ParticipantPlanAccountData",
      AggregateAllocationsStubData.ParticipantPlanAccountsData()
    );

    // Call rule
    this.nodeRules = new NodeRules(AggregateAllocationsRule, {
      ignoreFactChanges: true
    });
    it("AggregateAllocations return empty arrays since EDWIN errored out", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let UnweightedAverage = result.get("UnweightedAverage");
        expect(AggregateSources.data.length).to.equal(0);
        expect(UnweightedAverage.data.length).to.equal(0);
      });
    });
  });

  describe("Scenario 6: TM/SWIPR MS did respond, with a pending 'investmentelections' change and sources listed individually", () => {
    let rulesInput = new hashmap();
    // Set all MS inside rules input so we can pass these to rule

    rulesInput.set(
      "PendingInvestmentElectionsData",
      AggregateAllocationsStubData.s6_PendingInvestmentElectionsData()
    );
    rulesInput.set(
      "InvestmentsData",
      AggregateAllocationsStubData.InvestmentsData()
    );
    rulesInput.set(
      "PlanSourcesData",
      AggregateAllocationsStubData.PlanSourcesData()
    );
    rulesInput.set(
      "ParticipantPlanInvestmentAllocationsData",
      AggregateAllocationsStubData.ParticipantPlanInvestmentAllocationsData()
    );
    rulesInput.set(
      "ParticipantPlanAccountData",
      AggregateAllocationsStubData.ParticipantPlanAccountsData()
    );

    // Call rule
    this.nodeRules = new NodeRules(AggregateAllocationsRule, {
      ignoreFactChanges: true
    });
    it("AggregateAllocations rule should return UnweightedAverage with the proper fields and classes", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let UnweightedAverage = result.get("UnweightedAverage");
        expect(AggregateSources.data.length).to.equal(5);
        expect(UnweightedAverage.data.length).to.greaterThan(0);
        let hasProps = false;
        let firstElement = UnweightedAverage.data[0];
        // Check if Unweighted Average has the fields that it should
        if (
          firstElement.assetClassDesc &&
          firstElement.assetClassAllocationTotal &&
          firstElement.CategoryAllocations &&
          firstElement.CategoryAllocations[0].assetCategoryGroupName &&
          firstElement.CategoryAllocations[0].assetCategoryGroupAllocationTotal
        ) {
          hasProps = true;
        }
        expect(hasProps).to.equal(true);
      });
    });
    it("AggregateAllocations rule should return UnweightedAverage with source 'B' having 100% in the 'Bond' assetClass", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let UnweightedAverage = result.get("UnweightedAverage");
        let bondInfo = UnweightedAverage.data.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(100);
      });
    });
    it("AggregateAllocations rule should return AggregateSources array with 5 element", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        expect(AggregateSources.data.length).to.equal(5);
      });
    });
    it("AggregateAllocations rule should return AggregateSources with source 'B' having '100%' in the 'Bond' assetClassDesc", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let singleSource = AggregateSources.data.filter(
          source => source.sourceTypeCd == "B"
        );
        let bondInfo = singleSource[0].ClassAllocations.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(100);
      });
    });
    it("AggregateAllocations rule should return Investments for source '9' having '50%' where agreementInvestmenProductId = 106674", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let Investments = result.get("Investments");
        let singleInvestment = Investments.data.filter(source => {
          return source.agreementInvestmentProductId == "121809";
        });
        let investmentSourceInfo = singleInvestment[0].InvestmentSources.filter(
          source => {
            return source.sourceTypeCd == "9";
          }
        );
        expect(investmentSourceInfo[0].investmentElectionPct).to.equal(100);
      });
    });
    it("AggregateAllocations rule should return lastAllocationChangeDt from EDWIN of 2020-05-12", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let lastAllocationChangeDt = result.get(
          "LastAllocationPercentageChangeDt"
        );
        expect(lastAllocationChangeDt.data).to.equal("2020-05-12T18:31:57Z");
      });
    });
  });

  describe("Scenario 7: TM/SWIPR did respond, with a pending 'rebalance' change and sources listed invidually", () => {
    let rulesInput = new hashmap();
    // Set all MS inside rules input so we can pass these to rule

    rulesInput.set(
      "PendingInvestmentElectionsData",
      AggregateAllocationsStubData.s7_PendingInvestmentElectionsData()
    );
    rulesInput.set(
      "InvestmentsData",
      AggregateAllocationsStubData.InvestmentsData()
    );
    rulesInput.set(
      "PlanSourcesData",
      AggregateAllocationsStubData.PlanSourcesData()
    );
    rulesInput.set(
      "ParticipantPlanInvestmentAllocationsData",
      AggregateAllocationsStubData.ParticipantPlanInvestmentAllocationsData()
    );
    rulesInput.set(
      "ParticipantPlanAccountData",
      AggregateAllocationsStubData.ParticipantPlanAccountsData()
    );

    // Call rule
    this.nodeRules = new NodeRules(AggregateAllocationsRule, {
      ignoreFactChanges: true
    });
    it("AggregateAllocations rule should return UnweightedAverage with the proper fields and classes", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let UnweightedAverage = result.get("UnweightedAverage");
        expect(AggregateSources.data.length).to.equal(5);
        expect(UnweightedAverage.data.length).to.greaterThan(0);
        let hasProps = false;
        let firstElement = UnweightedAverage.data[0];
        // Check if Unweighted Average has the fields that it should
        if (
          firstElement.assetClassDesc &&
          firstElement.assetClassAllocationTotal &&
          firstElement.CategoryAllocations &&
          firstElement.CategoryAllocations[0].assetCategoryGroupName &&
          firstElement.CategoryAllocations[0].assetCategoryGroupAllocationTotal
        ) {
          hasProps = true;
        }
        expect(hasProps).to.equal(true);
      });
    });
    it("AggregateAllocations rule should return UnweightedAverage with source 'B' having '80%' in the 'Bond' assetClass", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let UnweightedAverage = result.get("UnweightedAverage");
        let bondInfo = UnweightedAverage.data.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(80);
      });
    });
    it("AggregateAllocations rule should return AggregateSources array with 5 element", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        expect(AggregateSources.data.length).to.equal(5);
      });
    });
    it("AggregateAllocations rule should return AggregateSources with source 'B' having '80%' in the 'Bond' assetClassDesc", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let singleSource = AggregateSources.data.filter(
          source => source.sourceTypeCd == "B"
        );
        let bondInfo = singleSource[0].ClassAllocations.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(80);
      });
    });
    it("AggregateAllocations rule should return Investments for source '9' having '50%' where agreementInvestmenProductId = 106674", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let Investments = result.get("Investments");
        let singleInvestment = Investments.data.filter(source => {
          return source.agreementInvestmentProductId == "121809";
        });
        let investmentSourceInfo = singleInvestment[0].InvestmentSources.filter(
          source => {
            return source.sourceTypeCd == "9";
          }
        );
        expect(investmentSourceInfo[0].investmentElectionPct).to.equal(80);
      });
    });
    it("AggregateAllocations rule should return lastAllocationChangeDt from EDWIN of 2020-05-14", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let lastAllocationChangeDt = result.get(
          "LastAllocationPercentageChangeDt"
        );
        expect(lastAllocationChangeDt.data).to.equal("2020-05-14T15:22:04Z");
      });
    });
  });

  describe("Scenario 8: TM/SWIPR did respond, with a pending 'rebalance' change and source '*'", () => {
    let rulesInput = new hashmap();
    // Set all MS inside rules input so we can pass these to rule

    rulesInput.set(
      "PendingInvestmentElectionsData",
      AggregateAllocationsStubData.s8_PendingInvestmentElectionsData()
    );
    rulesInput.set(
      "InvestmentsData",
      AggregateAllocationsStubData.InvestmentsData()
    );
    rulesInput.set(
      "PlanSourcesData",
      AggregateAllocationsStubData.PlanSourcesData()
    );
    rulesInput.set(
      "ParticipantPlanInvestmentAllocationsData",
      AggregateAllocationsStubData.ParticipantPlanInvestmentAllocationsData()
    );
    rulesInput.set(
      "ParticipantPlanAccountData",
      AggregateAllocationsStubData.ParticipantPlanAccountsData()
    );

    // Call rule
    this.nodeRules = new NodeRules(AggregateAllocationsRule, {
      ignoreFactChanges: true
    });
    it("AggregateAllocations rule should return UnweightedAverage with the proper fields and classes", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let UnweightedAverage = result.get("UnweightedAverage");
        expect(AggregateSources.data.length).to.equal(5);
        expect(UnweightedAverage.data.length).to.greaterThan(0);
        let hasProps = false;
        let firstElement = UnweightedAverage.data[0];
        // Check if Unweighted Average has the fields that it should
        if (
          firstElement.assetClassDesc &&
          firstElement.assetClassAllocationTotal &&
          firstElement.CategoryAllocations &&
          firstElement.CategoryAllocations[0].assetCategoryGroupName &&
          firstElement.CategoryAllocations[0].assetCategoryGroupAllocationTotal
        ) {
          hasProps = true;
        }
        expect(hasProps).to.equal(true);
      });
    });
    it("AggregateAllocations rule should return UnweightedAverage with source 'B' having '95%' in the 'Bond' assetClass", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let UnweightedAverage = result.get("UnweightedAverage");
        let bondInfo = UnweightedAverage.data.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(95);
      });
    });
    it("AggregateAllocations rule should return AggregateSources array with 5 element", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        expect(AggregateSources.data.length).to.equal(5);
      });
    });
    it("AggregateAllocations rule should return AggregateSources with source 'B' having '95%' in the 'Bond' assetClassDesc", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let singleSource = AggregateSources.data.filter(
          source => source.sourceTypeCd == "B"
        );
        let bondInfo = singleSource[0].ClassAllocations.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(95);
      });
    });
    it("AggregateAllocations rule should return Investments for source '9' having '50%' where agreementInvestmenProductId = 106674", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let Investments = result.get("Investments");
        let singleInvestment = Investments.data.filter(source => {
          return source.agreementInvestmentProductId == "121809";
        });
        let investmentSourceInfo = singleInvestment[0].InvestmentSources.filter(
          source => {
            return source.sourceTypeCd == "9";
          }
        );
        expect(investmentSourceInfo[0].investmentElectionPct).to.equal(95);
      });
    });
    it("AggregateAllocations rule should return lastAllocationChangeDt from EDWIN of 2020-05-14", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let lastAllocationChangeDt = result.get(
          "LastAllocationPercentageChangeDt"
        );
        expect(lastAllocationChangeDt.data).to.equal("2020-05-14T17:30:28Z");
      });
    });
  });

  describe("Scenario 9: TM/SWIPR MS did respond, with a pending 'allocationmodels', which is always done for all sources", () => {
    let rulesInput = new hashmap();
    // Set all MS inside rules input so we can pass these to rule

    rulesInput.set(
      "PendingInvestmentElectionsData",
      AggregateAllocationsStubData.s9_PendingInvestmentElectionsData()
    );
    rulesInput.set(
      "InvestmentsData",
      AggregateAllocationsStubData.InvestmentsData()
    );
    rulesInput.set(
      "PlanSourcesData",
      AggregateAllocationsStubData.PlanSourcesData()
    );
    rulesInput.set(
      "ParticipantPlanInvestmentAllocationsData",
      AggregateAllocationsStubData.ParticipantPlanInvestmentAllocationsData()
    );
    rulesInput.set(
      "ParticipantPlanAccountData",
      AggregateAllocationsStubData.ParticipantPlanAccountsData()
    );

    // Call rule
    this.nodeRules = new NodeRules(AggregateAllocationsRule, {
      ignoreFactChanges: true
    });
    it("AggregateAllocations rule should return UnweightedAverage with the proper fields and classes", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let UnweightedAverage = result.get("UnweightedAverage");
        expect(AggregateSources.data.length).to.equal(5);
        expect(UnweightedAverage.data.length).to.greaterThan(0);
        let hasProps = false;
        let firstElement = UnweightedAverage.data[0];
        // Check if Unweighted Average has the fields that it should
        if (
          firstElement.assetClassDesc &&
          firstElement.assetClassAllocationTotal &&
          firstElement.CategoryAllocations &&
          firstElement.CategoryAllocations[0].assetCategoryGroupName &&
          firstElement.CategoryAllocations[0].assetCategoryGroupAllocationTotal
        ) {
          hasProps = true;
        }
        expect(hasProps).to.equal(true);
      });
    });
    it("AggregateAllocations rule should return UnweightedAverage with source 'B' having '10%' in the 'Bond' assetClass", async () => {
      // Get rule response
      this.nodeRules.execute(rulesInput, function(result) {
        let UnweightedAverage = result.get("UnweightedAverage");
        let bondInfo = UnweightedAverage.data.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(10);
      });
    });
    it("AggregateAllocations rule should return AggregateSources array with 5 element", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        expect(AggregateSources.data.length).to.equal(5);
      });
    });
    it("AggregateAllocations rule should return AggregateSources with source 'B' having '10%' in the 'Bond' assetClassDesc", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let AggregateSources = result.get("AggregateSources");
        let singleSource = AggregateSources.data.filter(
          source => source.sourceTypeCd == "B"
        );
        let bondInfo = singleSource[0].ClassAllocations.filter(assetClass => {
          return assetClass.assetClassDesc == "Bond";
        });
        expect(bondInfo[0].assetClassAllocationTotal).to.equal(10);
      });
    });
    it("AggregateAllocations rule should return lastAllocationChangeDt from EDWIN of2020-05-14", async () => {
      this.nodeRules.execute(rulesInput, function(result) {
        let lastAllocationChangeDt = result.get(
          "LastAllocationPercentageChangeDt"
        );
        expect(lastAllocationChangeDt.data).to.equal("2020-05-14T15:33:21Z");
      });
    });
  });
});