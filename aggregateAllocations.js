const controllerUtils = require("../../utils/controllerUtils");
const logger = require("../logging");

module.exports = [
  {
    name: "aggregateAllocations",
    on: true,
    priority: 100,
    condition: function(R) {
      R.when(true);
    },
    consequence: controllerUtils.safeRuleWrapperAsync(async function(R) {
      // First we need to check for the pending transactions in SWIPR
      // we are ok with 2 scenarios. Success and NotFoundError
      // Any other response will result in an error
      let PendingInvestmentElectionsData = this.get(
        "PendingInvestmentElectionsData"
      );
      if (
        !PendingInvestmentElectionsData.success &&
        PendingInvestmentElectionsData.message !== "Not Found"
      ) {
        logger.error("There was an error retrieving pending transactions");
        this.set("AggregateSources", { data: [] });
        this.set("UnweightedAverage", { data: [] });
        R.stop();
      } else {
        // Check that we have 'success' on all the responses we need
        if (
          this.get("InvestmentsData").success &&
          this.get("PlanSourcesData").success &&
          this.get("ParticipantPlanInvestmentAllocationsData").success &&
          this.get("ParticipantPlanAccountData").success
        ) {
          let InvestmentsData = this.get("InvestmentsData");
          let PlanSourcesData = this.get("PlanSourcesData");
          let InvestmentElectionData = this.get(
            "ParticipantPlanInvestmentAllocationsData"
          );
          let PlanAccountsData = this.get("ParticipantPlanAccountData");
          // We have all successfull responses, start doing logic
          // Which we will first be checking if there is a pending transaction,
          // if so we will not even look at EDWIN for allocations
          if (PendingInvestmentElectionsData.success) {
            // we have pending investment election changes, handle them here
            // Check if there is an actual pending transaction, need to filter out non-pending transactions
            let mostRecentPendingTransaction = getMostRecentPendingElectionChange(
              PendingInvestmentElectionsData
            );
            // We now have the most recent pending investment election, or an empty array
            if (mostRecentPendingTransaction) {
              // If we got here, we have a pending transaction, we need to check what type of transaction it was
              // it could be 'reblance', 'investmentelections', or 'allocationmodels'
              let pendingData;
              // We now have the most recent valid pending transaction, use the submission date from this
              this.set("LastAllocationPercentageChangeDt", {
                data:
                  mostRecentPendingTransaction.PendingTransactionSubmittedDttm
              });
              switch (mostRecentPendingTransaction.PendingTransactionTypeCd) {
                case "rebalance":
                  pendingData =
                    mostRecentPendingTransaction.RequestBody.RebalanceSources
                      .Sources;
                  break;
                case "investmentelections":
                  pendingData =
                    mostRecentPendingTransaction.RequestBody
                      .InvestmentElectionSources.Source;
                  break;
                case "allocationmodels":
                  pendingData =
                    mostRecentPendingTransaction.RequestBody
                      .InvestmentElections;
                  break;
              }
              // Use the pendingData that was set in the switch
              // Check if pending transaction was done with source "*", if so we need to pull
              // plan sources and contruct the AggregateAllocations using EDWIN and SWIPR data
              // There are no sources to check for 'allocationmodels' transactions, so we know
              // we need to use createAggregateSourceFromSingleSource
              if (
                mostRecentPendingTransaction.PendingTransactionTypeCd ==
                "allocationmodels"
              ) {
                let AggregateSources = createAggregateSourcesFromPendingAAM(
                  pendingData,
                  PlanSourcesData,
                  InvestmentsData
                );
                this.set("AggregateSources", { data: AggregateSources });
                let UnweightedAverage = unweightedAverage(AggregateSources);
                this.set("UnweightedAverage", { data: UnweightedAverage });
                let Investments = transformAggregateSourcesIntoInvestmentsClass(
                  AggregateSources,
                  PlanSourcesData
                );
                this.set("Investments", { data: Investments });
                R.stop();
              } else if (pendingData[0].sourceTypeCd == "*") {
                let AggregateSources = createAggregateSourceFromSingleSource(
                  pendingData,
                  PlanSourcesData,
                  InvestmentsData
                );
                this.set("AggregateSources", { data: AggregateSources });
                let UnweightedAverage = unweightedAverage(AggregateSources);
                this.set("UnweightedAverage", { data: UnweightedAverage });
                let Investments = transformAggregateSourcesIntoInvestmentsClass(
                  AggregateSources,
                  PlanSourcesData
                );
                this.set("Investments", { data: Investments });
                R.stop();
              } else {
                // We can take the sources directly from the pending transaction, since they listed the each source;
                // Create the 'AggregateSources' response and pass it into the unweightedAverage function like above
                let AggregateSources = createAggregateSourceFromMultipleSources(
                  pendingData,
                  InvestmentsData
                );
                this.set("AggregateSources", { data: AggregateSources });
                let UnweightedAverage = unweightedAverage(AggregateSources);
                this.set("UnweightedAverage", { data: UnweightedAverage });
                let Investments = transformAggregateSourcesIntoInvestmentsClass(
                  AggregateSources,
                  PlanSourcesData
                );
                this.set("Investments", { data: Investments });
                R.stop();
              }
            } else {
              // We know we have no pending, set the last allocation change data using EDWIN data.
              this.set("LastAllocationPercentageChangeDt", {
                data: PlanAccountsData.data[0].lastAllocationPercentageChangeDt
              });
              // Pending was a success, but there were no actual pending investment elections in SWIPR
              // So we actually need to use the values from EDWIN
              let AggregateSources = createAggregateSourcesFromEDWIN(
                InvestmentsData,
                InvestmentElectionData
              );
              this.set("AggregateSources", { data: AggregateSources });
              let UnweightedAverage = unweightedAverage(AggregateSources);
              this.set("UnweightedAverage", { data: UnweightedAverage });
              let Investments = transformAggregateSourcesIntoInvestmentsClass(
                AggregateSources,
                PlanSourcesData
              );
              this.set("Investments", { data: Investments });
              R.stop();
            }
          } else {
            // Pending responded with not found, so there were no pending investment election transX
            // So we will have to pull from EDWIN
            // Set the last allocation change data using EDWIN data.
            this.set("LastAllocationPercentageChangeDt", {
              data: PlanAccountsData.data[0].lastAllocationPercentageChangeDt
            });
            let AggregateSources = createAggregateSourcesFromEDWIN(
              InvestmentsData,
              InvestmentElectionData
            );
            this.set("AggregateSources", { data: AggregateSources });
            let UnweightedAverage = unweightedAverage(AggregateSources);
            this.set("UnweightedAverage", { data: UnweightedAverage });
            let Investments = transformAggregateSourcesIntoInvestmentsClass(
              AggregateSources,
              PlanSourcesData
            );
            this.set("Investments", { data: Investments });
            R.stop();
          }
        } else {
          logger.error(
            "There was an error with one of the MS responses used in this rule"
          );
          this.set("AggregateSources", { data: [] });
          this.set("UnweightedAverage", { data: [] });
          R.stop();
          // MS Responses did not provide neccesary information
        }
      }
    })
  }
];

// This function takes in an array of aggregated sources, this arary should have
// the exact structure of the AggregateSources definition in the /investments/aggregate balances route
// This will return an unweighted average of assetClassDesc and assetCategoryGroupName
function unweightedAverage(aggregateSources) {
  let numOfSources = aggregateSources.length;
  let UnweightedAverageMap = {};
  aggregateSources.forEach(source => {
    source.ClassAllocations.forEach(classAllocation => {
      // Handle edge case for assetClassDesc
      /* istanbul ignore else*/
      if (!UnweightedAverageMap[classAllocation.assetClassDesc]) {
        UnweightedAverageMap[classAllocation.assetClassDesc] = {
          assetClassAllocationTotal: 0,
          assetCategoryGroupName: {}
        };
      }
      // Add allocation for assetClassDesc
      UnweightedAverageMap[
        classAllocation.assetClassDesc
      ].assetClassAllocationTotal += classAllocation.assetClassAllocationTotal;
      // Handle edge case for assetClass
      /* istanbul ignore else*/
      classAllocation.CategoryAllocations.forEach(categoryGroup => {
        if (
          !UnweightedAverageMap[classAllocation.assetClassDesc]
            .assetCategoryGroupName[categoryGroup.assetCategoryGroupName]
        ) {
          UnweightedAverageMap[
            classAllocation.assetClassDesc
          ].assetCategoryGroupName[categoryGroup.assetCategoryGroupName] = 0;
        }
        UnweightedAverageMap[
          classAllocation.assetClassDesc
        ].assetCategoryGroupName[categoryGroup.assetCategoryGroupName] +=
          categoryGroup.assetCategoryGroupAllocationTotal;
      });
    });
  });
  // At this point we have all sources summed up into one object
  // Now we just need to divide each election field by the number of sources in order
  // to get the unweighted average
  let UnweightedAverage = [];
  for (let [assetClass, assetClassInfo] of Object.entries(
    UnweightedAverageMap
  )) {
    let newEl = {
      assetClassDesc: assetClass,
      assetClassAllocationTotal:
        UnweightedAverageMap[assetClass].assetClassAllocationTotal /
        numOfSources,
      CategoryAllocations: []
    };
    for (let [assetCategory] of Object.entries(
      assetClassInfo.assetCategoryGroupName
    )) {
      newEl.CategoryAllocations.push({
        assetCategoryGroupName: assetCategory,
        assetCategoryGroupAllocationTotal: Number(
          toFixed(
            UnweightedAverageMap[assetClass].assetCategoryGroupName[
              assetCategory
            ] / numOfSources,
            2
          )
        )
      });
    }
    UnweightedAverage.push(newEl);
  }
  return UnweightedAverage;
}

// Since SWIPR will does not cancel previous investment elections if you submit multiple in a day
// there is the potential for there to be mutliple pending investment elections, OMNI will only take the latest
// so we are grabbing the most recently submitted transaction or returning nothing
function getMostRecentPendingElectionChange(pendingInvestmentElectionsData) {
  let realPendingTransaction;
  for (let i = 0; i < pendingInvestmentElectionsData.data.length; i++) {
    if (
      pendingInvestmentElectionsData.data[i].PendingTransactionLatestStatusCd ==
        "SUBMITTED" &&
      (pendingInvestmentElectionsData.data[i].PendingTransactionTypeCd !==
        "rebalance" ||
        (pendingInvestmentElectionsData.data[i].PendingTransactionTypeCd ==
          "rebalance" &&
          pendingInvestmentElectionsData.data[i].RequestBody.RebalanceSources
            .Sources[0].resetInvestmentElectionsInd))
    ) {
      realPendingTransaction = pendingInvestmentElectionsData.data[i];
      // We only want the latest, save it and break
      break;
    }
  }
  return realPendingTransaction;
}

// This function will take a pending transaction of type 'investmentelections' from TM MS,
// that was submitted with only
// a single source "*". It will return an aggregation of investment election % by assetClassDesc and assetCategoryGroupName
// for each source in the PlanSources MS response
function createAggregateSourceFromSingleSource(
  pendingData,
  planSourcesData,
  investmentsData
) {
  // Each source has the same allocation, so we need to aggregate the allocations once
  // To do this we will need a map of investments so we can quickly grab investment info
  let invMap = {};
  investmentsData.data.forEach(inv => {
    invMap[inv.agreementInvestmentProductId] = inv;
  });
  // Now we will go through each allocation on the pending transaction, and start aggregating allocations
  // by assetClass and assetCategoryGroup
  let allocationMap = {};
  pendingData[0].Allocations.forEach(allocation => {
    let invInfo = invMap[allocation.agreementInvestmentProductId];
    // Check if class exists in map, edge case
    /* istanbul ignore else*/
    if (!allocationMap[invInfo.assetClassDesc]) {
      allocationMap[invInfo.assetClassDesc] = {
        assetClassDesc: invInfo.assetClassDesc,
        assetClassAllocationTotal: 0,
        CategoryAllocations: {}
      };
    }
    // Add class allocation
    allocationMap[invInfo.assetClassDesc].assetClassAllocationTotal +=
      allocation.investmentElectionPct;
    // Check if category exists under class yet, edge case
    /* istanbul ignore else*/
    if (
      !allocationMap[invInfo.assetClassDesc].CategoryAllocations[
        invInfo.assetCategoryGroupName
      ]
    ) {
      allocationMap[invInfo.assetClassDesc].CategoryAllocations[
        invInfo.assetCategoryGroupName
      ] = {
        assetCategoryGroupAllocationTotal: 0,
        InvestmentAllocations: {}
      };
    }
    // Add categoryGroup allocation
    allocationMap[invInfo.assetClassDesc].CategoryAllocations[
      invInfo.assetCategoryGroupName
    ].assetCategoryGroupAllocationTotal += allocation.investmentElectionPct;
    // Dont need an edge case, just add investment underneath
    allocationMap[invInfo.assetClassDesc].CategoryAllocations[
      invInfo.assetCategoryGroupName
    ].InvestmentAllocations[allocation.agreementInvestmentProductId] =
      allocation.investmentElectionPct;
  });

  // Call function to transform allocatinoMap to an array, for each source in planSourcesData
  let AggregateSources = transformAllocationMapToArrayUsingPlanSources(
    allocationMap,
    planSourcesData,
    invMap
  );
  return AggregateSources;
}

// This function will aggregate the pending transaction of type 'investmentelections' from TM MS,
// using the sources listed on the transaction
// It will return an aggregation of investment election % by assetClassDesc and assetCategoryGroupName
// for each source in the pending transaction MS response
function createAggregateSourceFromMultipleSources(
  pendingData,
  investmentsData
) {
  // Each source has the same allocation, so we need to aggregate the allocations once
  // To do this we will need a map of investments so we can quickly grab investment info
  let invMap = {};
  investmentsData.data.forEach(inv => {
    invMap[inv.agreementInvestmentProductId] = inv;
  });
  // Loop through each source in pending, create our map to aggregate in
  let allocationMap = {};
  pendingData.forEach(pendingSource => {
    allocationMap[pendingSource.sourceTypeCd] = {};

    // Go through each allocation for each source
    pendingSource.Allocations.forEach(allocation => {
      let invInfo = invMap[allocation.agreementInvestmentProductId];
      // Handle edge case, add class
      /* istanbul ignore else*/
      if (!allocationMap[pendingSource.sourceTypeCd][invInfo.assetClassDesc]) {
        allocationMap[pendingSource.sourceTypeCd][invInfo.assetClassDesc] = {
          assetClassAllocationTotal: 0,
          CategoryAllocations: {}
        };
      }
      // Add values to class
      allocationMap[pendingSource.sourceTypeCd][
        invInfo.assetClassDesc
      ].assetClassAllocationTotal += allocation.investmentElectionPct;
      // Handle edge case, add category group under class
      /* istanbul ignore else*/
      if (
        !allocationMap[pendingSource.sourceTypeCd][invInfo.assetClassDesc]
          .CategoryAllocations[invInfo.assetCategoryGroupName]
      ) {
        allocationMap[pendingSource.sourceTypeCd][
          invInfo.assetClassDesc
        ].CategoryAllocations[invInfo.assetCategoryGroupName] = {
          assetCategoryGroupAllocationTotal: 0,
          InvestmentAllocations: {}
        };
      }
      // Add values to category
      allocationMap[pendingSource.sourceTypeCd][
        invInfo.assetClassDesc
      ].CategoryAllocations[
        invInfo.assetCategoryGroupName
      ].assetCategoryGroupAllocationTotal += allocation.investmentElectionPct;
      // Dont need to handle edge case as you should only have each investment/source once
      allocationMap[pendingSource.sourceTypeCd][
        invInfo.assetClassDesc
      ].CategoryAllocations[
        invInfo.assetCategoryGroupName
      ].InvestmentAllocations[allocation.agreementInvestmentProductId] =
        allocation.investmentElectionPct;
    });
  });
  // Turn allocationMap into array
  let AggregateSources = transformAllocationMapToArrayUsingSourcesListed(
    allocationMap,
    invMap
  );
  return AggregateSources;
}

// This function will aggregate your current investment ellections as they are in EDWIN. This function will only
// be called when you don't have any pending investment elections changes in SWIPR. This will return an aggregation of
// investment elections % by assetClassDesc and assetCategoryGroupName for all sources in the
// ParticipantPlanInvestmentAllocationsData response
function createAggregateSourcesFromEDWIN(
  investmentsData,
  investmentElectionData
) {
  // Create map of investment info
  let invMap = {};
  investmentsData.data.forEach(inv => {
    invMap[inv.agreementInvestmentProductId] = inv;
  });
  // Create allocation map and starts going through all EDWIN allocations
  let allocationMap = {};
  investmentElectionData.data.forEach(allocation => {
    //Get investment data
    let invInfo = invMap[allocation.agreementInvestmentProductId];
    // Create source in map, edge case
    if (!allocationMap[allocation.sourceTypeCd]) {
      allocationMap[allocation.sourceTypeCd] = {};
    }
    // Create class under source, edge case
    /* istanbul ignore else*/
    if (!allocationMap[allocation.sourceTypeCd][invInfo.assetClassDesc]) {
      allocationMap[allocation.sourceTypeCd][invInfo.assetClassDesc] = {
        assetClassAllocationTotal: 0,
        CategoryAllocations: {}
      };
    }
    // Add allocations to class
    /* istanbul ignore else*/
    allocationMap[allocation.sourceTypeCd][
      invInfo.assetClassDesc
    ].assetClassAllocationTotal += allocation.investmentElectionPct;
    // Create category under class, edge case
    /* istanbul ignore else*/
    if (
      !allocationMap[allocation.sourceTypeCd][invInfo.assetClassDesc]
        .CategoryAllocations[invInfo.assetCategoryGroupName]
    ) {
      allocationMap[allocation.sourceTypeCd][
        invInfo.assetClassDesc
      ].CategoryAllocations[invInfo.assetCategoryGroupName] = {
        assetCategoryGroupAllocationTotal: 0,
        InvestmentAllocations: {}
      };
    }
    // Add allocations to category
    allocationMap[allocation.sourceTypeCd][
      invInfo.assetClassDesc
    ].CategoryAllocations[
      invInfo.assetCategoryGroupName
    ].assetCategoryGroupAllocationTotal += allocation.investmentElectionPct;
    // Create investment under category, edge case
    if (
      !allocationMap[allocation.sourceTypeCd][invInfo.assetClassDesc]
        .CategoryAllocations[invInfo.assetCategoryGroupName]
        .InvestmentAllocations[allocation.agreementInvestmentProductId]
    ) {
      allocationMap[allocation.sourceTypeCd][
        invInfo.assetClassDesc
      ].CategoryAllocations[
        invInfo.assetCategoryGroupName
      ].InvestmentAllocations[allocation.agreementInvestmentProductId] = 0;
    }
    // Add allocations to class
    allocationMap[allocation.sourceTypeCd][
      invInfo.assetClassDesc
    ].CategoryAllocations[invInfo.assetCategoryGroupName].InvestmentAllocations[
      allocation.agreementInvestmentProductId
    ] += allocation.investmentElectionPct;
  });
  // We now have the allocationMap complete, now we need to turn this into an array.
  let AggregateSources = transformAllocationMapToArrayUsingSourcesListed(
    allocationMap,
    invMap
  );

  return AggregateSources;
}

// This function will  aggregate your current pending AAM investment election
function createAggregateSourcesFromPendingAAM(
  pendingData,
  planSourcesData,
  investmentsData
) {
  // Each source has the same allocation, so we need to aggregate the allocations once
  // To do this we will need a map of investments so we can quickly grab investment info
  let invMap = {};
  investmentsData.data.forEach(inv => {
    invMap[inv.agreementInvestmentProductId] = inv;
  });
  // Now we will go through each allocation on the pending transaction, and start aggregating allocations
  // by assetClass and assetCategoryGroup
  let allocationMap = {};
  // Build the allocations, then create an element in an array for each source in planSourcesData
  pendingData.forEach(inv => {
    let invInfo = invMap[inv.agreementInvestmentProductId];
    // Handle edge case for assetClass
    /* istanbul ignore else*/
    if (!allocationMap[invInfo.assetClassDesc]) {
      allocationMap[invInfo.assetClassDesc] = {
        assetClassDesc: invInfo.assetClassDesc,
        assetClassAllocationTotal: 0,
        CategoryAllocations: {}
      };
    }
    // Add assetClass
    allocationMap[invInfo.assetClassDesc].assetClassAllocationTotal +=
      inv.investmentElectionPct;
    // Handle edge case for assetCategoryGroup
    /* istanbul ignore else*/
    if (
      !allocationMap[invInfo.assetClassDesc].CategoryAllocations[
        invInfo.assetCategoryGroupName
      ]
    ) {
      allocationMap[invInfo.assetClassDesc].CategoryAllocations[
        invInfo.assetCategoryGroupName
      ] = {
        assetCategoryGroupAllocationTotal: 0,
        InvestmentAllocations: {}
      };
    }
    // Add assetCategoryGroup's allocation
    allocationMap[invInfo.assetClassDesc].CategoryAllocations[
      invInfo.assetCategoryGroupName
    ].assetCategoryGroupAllocationTotal += inv.investmentElectionPct;
    // Add Investment in InvestmentAllocations, don't need edge case as each investment/source combo
    // will only have one allocation
    allocationMap[invInfo.assetClassDesc].CategoryAllocations[
      invInfo.assetCategoryGroupName
    ].InvestmentAllocations[invInfo.agreementInvestmentProductId] =
      inv.investmentElectionPct;
  });
  // Transform the allocationMap into the AggregateSources array structure, using planSourcesData
  let AggregateSources = transformAllocationMapToArrayUsingPlanSources(
    allocationMap,
    planSourcesData,
    invMap
  );
  return AggregateSources;
}

// This will take the allocationMap that is created from a pendingTransaction submitted with source "*",
// It will create the AggregateSources class using the allocationMap, and will create an element within AggregateSources
// for every source in the planSourcesData array
function transformAllocationMapToArrayUsingPlanSources(
  allocationMap,
  planSourcesData,
  invMap
) {
  // Turn map into array based response that is needed for API output
  let ClassAllocations = [];
  for (let [, assetClassInfo] of Object.entries(allocationMap)) {
    let newEl = {
      assetClassDesc: assetClassInfo.assetClassDesc,
      assetClassAllocationTotal: assetClassInfo.assetClassAllocationTotal,
      CategoryAllocations: []
    };
    for (let [assetCategoryGroup, assetCategoryInfo] of Object.entries(
      assetClassInfo.CategoryAllocations
    )) {
      let newCategoryEl = {
        assetCategoryGroupName: assetCategoryGroup,
        assetCategoryGroupAllocationTotal:
          assetCategoryInfo.assetCategoryGroupAllocationTotal,
        InvestmentAllocations: []
      };
      for (let [invId, investmentElectionPct] of Object.entries(
        assetCategoryInfo.InvestmentAllocations
      )) {
        newCategoryEl.InvestmentAllocations.push({
          agreementInvestmentProductId: invId,
          hostInvestmentProductNum: invMap[invId].hostInvestmentProductNum,
          agreementInvestmentProductLongDesc:
            invMap[invId].agreementInvestmentProductLongDesc,
          assetCategoryGroupName: invMap[invId].assetCategoryGroupName,
          assetCategoryRankingNum: invMap[invId].assetCategoryRankingNum,
          assetClassDesc: invMap[invId].assetClassDesc,
          investmentElectionPct: investmentElectionPct
        });
      }
      newEl.CategoryAllocations.push(newCategoryEl);
    }
    ClassAllocations.push(newEl);
  }
  // Now we have the aggregageAllocations but they are not tied to any of the plan sources
  // Create an array and push an element for each plan source, using the aggregatedAllocations we just created
  let AggregateSources = [];
  planSourcesData.data.forEach(el => {
    AggregateSources.push({
      sourceTypeCd: el.sourceTypeCd,
      ClassAllocations
    });
  });
  return AggregateSources;
}

// This will take the allocationMap and turn it into AggregateSources array
function transformAllocationMapToArrayUsingSourcesListed(
  allocationMap,
  invMap
) {
  let AggregateSources = [];
  for (let [source, sourceInfo] of Object.entries(allocationMap)) {
    let newEl = {
      sourceTypeCd: source,
      ClassAllocations: []
    };
    for (let [assetClass, assetClassInfo] of Object.entries(sourceInfo)) {
      let newClassEl = {
        assetClassDesc: assetClass,
        assetClassAllocationTotal: assetClassInfo.assetClassAllocationTotal,
        CategoryAllocations: []
      };
      for (let [assetCategory, assetCategoryGroupInfo] of Object.entries(
        assetClassInfo.CategoryAllocations
      )) {
        let newCategoryEl = {
          assetCategoryGroupName: assetCategory,
          assetCategoryGroupAllocationTotal:
            assetCategoryGroupInfo.assetCategoryGroupAllocationTotal,
          InvestmentAllocations: []
        };
        for (let [invId, investmentElectionPct] of Object.entries(
          assetCategoryGroupInfo.InvestmentAllocations
        )) {
          let newInvestmentEl = {
            agreementInvestmentProductId: invId,
            hostInvestmentProductNum: invMap[invId].hostInvestmentProductNum,
            agreementInvestmentProductLongDesc:
              invMap[invId].agreementInvestmentProductLongDesc,
            assetCategoryGroupName: invMap[invId].assetCategoryGroupName,
            assetCategoryRankingNum: invMap[invId].assetCategoryRankingNum,
            assetClassDesc: invMap[invId].assetClassDesc,
            investmentElectionPct: investmentElectionPct
          };
          newCategoryEl.InvestmentAllocations.push(newInvestmentEl);
        }
        newClassEl.CategoryAllocations.push(newCategoryEl);
      }
      newEl.ClassAllocations.push(newClassEl);
    }
    AggregateSources.push(newEl);
  }
  return AggregateSources;
}

// This will reformat AggregateSources the Investments class
function transformAggregateSourcesIntoInvestmentsClass(
  aggregateSources,
  planSourcesData
) {
  let planSourceMap = {};
  planSourcesData.data.forEach(source => {
    planSourceMap[source.sourceTypeCd] = source;
  });
  let InvestmentsMap = {};
  aggregateSources.forEach(source => {
    source.ClassAllocations.forEach(assetClass => {
      assetClass.CategoryAllocations.forEach(assetCategory => {
        assetCategory.InvestmentAllocations.forEach(investment => {
          // Check to see if we have dealt with this investment before, if not handle edge
          if (!InvestmentsMap[investment.agreementInvestmentProductId]) {
            InvestmentsMap[investment.agreementInvestmentProductId] = {
              agreementInvestmentProductId:
                investment.agreementInvestmentProductId,
              hostInvestmentProductNum: investment.hostInvestmentProductNum,
              agreementInvestmentProductLongDesc:
                investment.agreementInvestmentProductLongDesc,
              assetCategoryGroupName: investment.assetCategoryGroupName,
              assetCategoryRankingNum: investment.assetCategoryRankingNum,
              assetClassDesc: investment.assetClassDesc,
              InvestmentSources: []
            };
          }
          // Now add the source allocation underneath
          InvestmentsMap[
            investment.agreementInvestmentProductId
          ].InvestmentSources.push({
            sourceTypeCd: source.sourceTypeCd,
            sourceTypeDesc: planSourceMap[source.sourceTypeCd].sourceTypeDesc,
            agreementSourceName:
              planSourceMap[source.sourceTypeCd].agreementSourceName,
            investmentElectionPct: investment.investmentElectionPct
          });
        });
      });
    });
  });
  let Investments = [];
  for (let [, element] of Object.entries(InvestmentsMap)) {
    Investments.push(element);
  }
  return Investments;
}

function toFixed(number, decimals) {
  var x = Math.pow(10, Number(decimals) + 1);
  return (Number(number) + 1 / x).toFixed(decimals);
}